/* "@(#) $Id$"

(c) 2002 Thomas Juerges <thomas.juerges@astro.ruhr-uni-bochum.de>

Program to recalculate 1950.0 equinox coordinates to 2000.0 equinox
coordinates.

*/

#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <iomanip>
#include <string>
#include <cmath>


int main(int argc __attribute__((unused)), char *argv[] __attribute__((unused)))
{
    const long double MulPi180(180. * M_PIl);
	const long double RA(46.1244 * 50. / MulPi180);
	const long double DEC(20.0431 * 50. / MulPi180);

	std::string buff;
	std::ifstream in("qso_mm.txt", std::ios::in);
	std::ofstream out("neu.txt");
	out.setf(std::ios::right);
	out.fill(' ');

	while(!in.eof())
	{
	    std::string name,type;
	    long double RAh, RAm, Dg, Dm;
		long double RAs, Ds, D, R, mag, dummy;
		long double delta_alpha, delta_delta;
		long double _R, _D;

		in >> name
            >> RAh
            >> RAm
            >> RAs
            >> Dg
            >> Dm
            >> Ds
            >> type
            >> mag
            >> dummy;

		std::cout << "Name: "
            << name
            << " alpha = "
            << RAh
            << "h "
            << RAm
            << "m "
            << RAs
            << "s delta = "
            << Dg
            << "g "
            << Dm
            << "\' "
            << Ds
            << "\" T = "
            << type
            <<" mag"
            << mag
            << " "
            << dummy
            << "\n";

		R = (RAh * 3600. + RAm * 60. + RAs) * 15.; //Ab hier: R in Bogensekunden
		D = std::abs(Dg * 3600.);
		D += Dm * 60. + Ds; //Ab hier: D in Bogensekunden
		if(Dg < 0) //Falls Deklination<0
		{
		    D = -D;
		}

		_D = D / 3600. * M_PI / 180.; //_D/3600=D in Grad *PI/180=D in Bogenmass
		_R = R / 3600. * M_PI / 180.; //_R/3600=R in Grad *PI/180=R in Bogenmass

		delta_alpha = RA + DEC * ::tan(_D) * ::sin(_R); //Voigt, S.19
		delta_delta = DEC * cos(_R); //Voigt, S.19

		delta_alpha = delta_alpha / MulPi180; //Zurueck zu Grad
		delta_delta = delta_delta / MulPi180;

		R += delta_alpha;
		D += delta_delta;

		R /= 15.;	//Zurueck zu 24h-Darstellung

		int rh(static_cast< int >(R / 3600.));
		int rm(static_cast< int >(
		    (R - static_cast< long double >(rh) * 3600.) / 60.));
		long double rs(R - rh * 3600. - rm * 60.);

		int	dg(static_cast< int >(D / 3600.));
		int dm(static_cast< int >(
		    (D - static_cast< long double >(dg) * 3600.) / 60.));
		long double ds(D - (static_cast< long double >(dg) * 3600.)
		    - (static_cast< long double >(dm) * 60.));

		if(Dg < 0)
        {
            dm = -dm;
            ds = -ds;
        }

		std::cout << "Name: "
            << name
            << " alpha = "
            << rh
            << "h "
            << rm
            << "m "
            << rs
            << "s delta = "
            << dg
            << "g "
            << dm
            << "\' "
            << ds
            << "\" T = "
            << type
            << " mag"
            << mag
            << " "
            << dummy
            << "\n"
            << "--------------------------------------\n";

            out << name
            << "\t"
            << std::setw(2)
            << rh
            << " "
            << std::setw(2)
            << rm
            << " "
            << std::setprecision(5)
            << rs
            << "\t"
            << std::setw(2)
            << dg
            << " "
            << std::setw(2)
            << dm
            << " "
            << std::setprecision(5)
            << ds
            << "\t"
            << type
            << "\t"
            << mag
            << "\t"
            << dummy
            << "\n";
	};

	in.close();
	out.close();
}
